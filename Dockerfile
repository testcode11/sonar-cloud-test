# Define the "build" stage
FROM maven:3.8.3-jdk-11 AS build

WORKDIR /app
RUN mvn --version
COPY pom.xml .
RUN mvn dependency:go-offline
COPY src ./src
RUN mvn package -DskipTests

# Define the "run" stage
FROM openjdk:11-jre-slim

WORKDIR /app
RUN mvn package
COPY --from=build /app/target/myapp.jar .

CMD ["java", "-jar", "myapp.jar"]
